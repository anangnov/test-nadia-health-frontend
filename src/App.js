import React, { Component } from "react";
import Header from './components/Header';
import Footer from './components/Footer';
import Sidebar from './components/Sidebar';
import Collapsible from 'react-collapsible';
import Loader from "react-js-loader";
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props);
    this._handleEnter = this._handleEnter.bind(this);

    this.state = {
      job: [],
      loading: true,
      filter: {}
    }
  }

  _handleEnter = async (e) => {
    if (e.key === 'Enter') {
      const search = e.target.value;
      
      this.setState({
        search: search,
        loading: true
      });

      await axios.get(`${process.env.REACT_APP_API_URL}/jobs/search?job_title=${search}`)
        .then((res) => {
          console.log(res)
          this.setState({
            job: res.data.data,
            loading: false
          })
        })
        .catch((err) => {
          console.log(err)
        })
    }
  }

  componentWillMount() {
    axios.get(`${process.env.REACT_APP_API_URL}/jobs/all`)
      .then((res) => {
        this.setState({ 
          loading: false,
          job: res.data.data
        })
      })
      .catch((err) => {
        console.log(err)
      })

    axios.get(`${process.env.REACT_APP_API_URL}/jobs/filters/all`)
      .then((res) => {
        this.setState({
          filter: res.data.data
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  render() {
    const { job, filter, loading } = this.state;
    console.log(filter.job_type)
    return(
      <div>
        <Header />
        <div className="container mx-auto px-5">
          <div className="mt-5 bg-white shadow p-2 flex rounded-sm">
            <span className="w-auto flex justify-end items-center text-gray-500 p-2">
              <i className="material-icons text-3xl">search</i>
            </span>
            <input 
              className="outline-none w-full rounded-sm p-3" 
              type="text" 
              onKeyDown={this._handleEnter}
              placeholder="Search for any job, title, keywords or company" 
            />
          </div>
          <div className="mt-5 grid grid-rows-12 grid-flow-col gap-4">
            <Sidebar />
            <div className="col-span-3">
              <div className="mb-5 bg-white rounded-sm p-4">
                {
                  loading ? (
                    <div className="mt-8 mb-8">
                      <Loader type="bubble-scale" bgColor={"#15d415"} size={60} />
                    </div>
                  ) : (
                    <div>
                      <p className="text-sm mb-4"><span className="font-bold">{job.length}</span> job postings</p>
                      <div>
                        {
                          job.map((key, i) => {
                            return(
                              <div key={i} className="p-5 border-2 rounded-md border-gray-100 hover:border-gray-500 cursor-pointer mb-3">
                                <Collapsible trigger={key.items.length + ' jobs for ' + key.name}>
                                  {
                                    key.items.map((key, i) => {
                                      return(
                                        <div key={i} className="mt-4 border-t pb-4 border-b">
                                          <p className="mt-3 text-black-500 font-bold">
                                            {key.job_title}
                                          </p>
                                          <p className="text-gray-500">
                                            {key.job_type + ' |  $' + key.salary_range[0] + ' - $' + key.salary_range[1] + ' | ' + key.address}
                                          </p>
                                          <p className="mt-5 text-gray-500">
                                            <div className="flex">
                                              <div className="flex-auto">
                                                <div className="flex mb-2">
                                                  <div className="flex mr-7">
                                                    <p className="text-black">Department: </p>
                                                  </div>
                                                  <div className="flex-1">
                                                    {key.department.map((key, i) => {
                                                      return(
                                                        <p key={i}>{key}</p>
                                                      )
                                                    })}
                                                  </div>
                                                </div>
                                                <div className="flex mb-2">
                                                  <div className="flex mr-7">
                                                    <p className="text-black">Hours/Shifts: </p>
                                                  </div>
                                                  <div className="flex-1">
                                                    {key.hours[0] + ' hours / ' + key.work_schedule}
                                                  </div>
                                                </div>
                                                <div className="flex mb-2">
                                                  <div className="flex mr-7">
                                                    <p className="text-black">Summary: </p>
                                                  </div>
                                                  <div className="flex-1">
                                                    {key.description}
                                                  </div>
                                                </div>
                                              </div>
                                              <div className="flex-auto">
                                                <div className="flex-auto text-right">
                                                  <div className="mt-2 mb-2">
                                                    <button className="text-xs bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded-md">
                                                      Job Details
                                                    </button>
                                                  </div>
                                                  <div className="mt-2 mb-2">
                                                    <button className="text-xs bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded-md">
                                                      Save Job
                                                    </button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </p>
                                        </div>
                                      )
                                    })
                                  }
                                  
                                  
                                </Collapsible>
                              </div>
                            )
                          })
                        }
                      </div>
                    </div>
                  )
                }
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

export default App;
