import React, { Component } from "react";
import axios from 'axios';

class Sidebar extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            jobType: [],
            experience: [],
            workSchedule: [],
            department: [],
        }
    }

    componentDidMount() {
        axios.get(`${process.env.REACT_APP_API_URL}/jobs/filters/all`)
          .then((res) => {
            this.setState({
                jobType: res.data.data.job_type,
                experience: res.data.data.experience,
                workSchedule: res.data.data.work_schedule,
                department: res.data.data.department
            })
          })
          .catch((err) => {
            console.log(err)
          })
    }

    render() {
        const { jobType, experience, workSchedule, department } = this.state;
        return(
            <div className="row-span-2">
                <div className="mb-5 bg-white rounded-sm p-4">
                <p className="text-sm font-bold">JOB TYPE</p>
                <div className="pt-3">
                    {
                        jobType.map((key, i) => {
                            return(
                                <p key={i} className="text-sm mb-2">{key.key} <span className="text-gray-300">{key.doc_count}</span></p>
                            )
                        })
                    }
                </div>
                </div>
                <div className="mb-5 bg-white rounded-sm p-4">
                <p className="text-sm font-bold">DEPARTMENT</p>
                <div className="pt-3">
                    {
                        department.map((key, i) => {
                            return(
                                <p key={i} className="text-sm mb-2">{key.key} <span className="text-gray-300">{key.doc_count}</span></p>
                            )
                        })
                    }
                </div>
                </div>
                <div className="mb-5 bg-white rounded-sm p-4">
                <p className="text-sm font-bold">WORK SCHEDULE</p>
                <div className="pt-3">
                    {
                        workSchedule.map((key, i) => {
                            return(
                                <p key={i} className="text-sm mb-2">{key.key} <span className="text-gray-300">{key.doc_count}</span></p>
                            )
                        })
                    }
                </div>
                </div>
                <div className="mb-5 bg-white rounded-sm p-4">
                <p className="text-sm font-bold">EXPERIENCE</p>
                <div className="pt-3">
                    {
                        experience.map((key, i) => {
                            return(
                                <p key={i} className="text-sm mb-2">{key.key} <span className="text-gray-300">{key.doc_count}</span></p>
                            )
                        })
                    }
                </div>
                </div>
            </div>
        )
    }
}

export default Sidebar;