const Footer = () => {
    return (
        <div className="flex flex-wrap justify-center bg-white p-6">
            <div className="flex flex-wrap mb-4 w-full">
                <div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/4 ">
                <h3 className="text-2xl py-4">About Us</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod. 
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
                    </p>
                </div>
                <div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/4 pl-8">
                    <h3 className="text-2xl py-4">Sitemap</h3>
                    <ul>
                        <li><a href="/">Nurses</a></li>
                        <li><a href="/">Employes</a></li>
                        <li><a href="/">Social Networking</a></li>
                        <li><a href="/">Jobs</a></li>
                    </ul>
                </div>
                <div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/4 ">
                    <h3 className="text-2xl py-4">Privacy</h3>
                    <ul>
                        <li><a href="/">Term of use</a></li>
                        <li><a href="/">Privacy policy</a></li>
                        <li><a href="/">Cookie policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    );
}
    
export default Footer;
    